﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EpamTask1.Models
{
    public class Ticket
    {
        public int Id { get; set; }
        public decimal Price { get; set; }

        public int? EventId { get; set; }
        public Event Event { get; set; }

       // [ForeignKey("Seller")]
        public int? UserId { get; set; }
        public User Seller { get; set; }

        public int? OrderId { get; set; }
        public Order Order { get; set; }

    }
}
