﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpamTask1.Models
{
    public class City
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<Venue> Venues { get; set; }
        public City()
        {
            Venues = new List<Venue>();
        }
    }
}
