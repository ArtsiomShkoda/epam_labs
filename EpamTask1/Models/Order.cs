﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpamTask1.Models
{
    public class Order
    {
        public int Id { get; set; }
        // Может быть и стринга.
        public int TrackNO { get; set; }

        public int? StatusId { get; set; }
        public OrderStatus OrderStatus { get; set; }

        public int? UserId { get; set; }
        public User Buyer { get; set; }

        public ICollection<Ticket> Tickets { get; set; }
        public Order()
        {
            Tickets = new List<Ticket>();
        }
    }
}
