﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpamTask1.Models
{
    public class User : IdentityUser
    {
        //public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Localization { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }

        public ICollection<Ticket> Tickets { get; set; }
        public ICollection<Order> Orders { get; set; }
        public User()
        {
            Tickets = new List<Ticket>();
            Orders = new List<Order>();
        }

    }
}
