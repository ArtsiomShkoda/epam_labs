﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpamTask1.Models
{
    public class Venue
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        // -------------------------------
        public int? СityId { get; set; }
        public City City { get; set; }

        public ICollection<Event> Events { get; set; }
        public Venue()
        {
            Events = new List<Event>();
        }

        public string GetAdress()
        {
            return String.Format("{0} {1} {2}", Name,Address,City.Name);
        }
    }
}
