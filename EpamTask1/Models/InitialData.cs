﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpamTask1.Models
{
    public static class InitialData
    {
        public static void Initialize(EventContext context)
        {
            #region City

            City testCity1 = new City
            {
                Name = "Brooklin1"
            };
            City testCity2 = new City
            {
                Name = "Brooklin2"
            };
            City testCity3 = new City
            {
                Name = "Brooklin3"
            };

            context.Cities.Add(testCity1);
            context.Cities.Add(testCity2);
            context.Cities.Add(testCity3);
            #endregion
            #region testVenue 
            Venue testVenue = new Venue
            {
                Name = "GCK",
                Address = "aaasss 12",
                City = testCity1
            };
            context.Venues.Add(testVenue);
            #endregion
            #region User
            User testUser1 = new User
            {
                FirstName = "Johnny",
                LastName = "Swamp Stomp",
                Address = "Louisiana",
                PhoneNumber = "375291356537",
                Localization = "RU"
            };
            User testUser2 = new User
            {
                FirstName = "Johnny",
                LastName = "Swamp Stomp",
                Address = "Louisiana",
                PhoneNumber = "375291356537",
                Localization = "RU"
            };
            context.Users.Add(testUser1);
            context.Users.Add(testUser2);
            #endregion
            #region Event
            Event testEvent = new Event
            {
                Name = "Delta Blues",
                Venue = testVenue,
                Banner = "/images/banner1.svg",
                Date = DateTime.UtcNow.AddDays(15f),
            };
            Event testEvent2 = new Event
            {
                Name = "AC/DC",
                Venue = testVenue,
                Banner = "/images/banner2.svg",
                Date = DateTime.UtcNow.AddMonths(3),
            };
            Event testEvent3 = new Event
            {
                Name = "Deep Purple",
                Banner = "/images/banner3.svg",
                Venue = testVenue,
                Date = DateTime.UtcNow.AddDays(5f),
            };
            Event testEvent4 = new Event
            {
                Name = "Country Paty",
                Venue = testVenue,
                Banner = "/images/banner4.svg",
                Date = DateTime.UtcNow.AddMonths(2),
            };
            context.Events.Add(testEvent);
            context.Events.Add(testEvent2);
            context.Events.Add(testEvent3);
            context.Events.Add(testEvent4);
            #endregion
            #region Ticket
            Ticket testTicket = new Ticket()
            {
                Price = 12.89m,
                Event = testEvent,
                Seller = testUser1,
            };
            Ticket testTicket2 = new Ticket()
            {
                Price = 12.89m,
                Event = testEvent2,
                Seller = testUser2,
            };
            Ticket testTicket3 = new Ticket()
            {
                Price = 12.89m,
                Event = testEvent,
                Seller = testUser1,
            };
            Ticket testTicket4 = new Ticket()
            {
                Price = 12.89m,
                Event = testEvent2,
                Seller = testUser2,
            };
            Ticket testTicket5 = new Ticket()
            {
                Price = 12.89m,
                Event = testEvent2,
                Seller = testUser2,
            };
            context.Tickets.Add(testTicket2);
            context.Tickets.Add(testTicket);
            context.Tickets.Add(testTicket3);
            context.Tickets.Add(testTicket4);
            context.Tickets.Add(testTicket);
            #endregion

            OrderStatus testStatusWaiting = new OrderStatus()
            {
                StatusType = "Waiting for confirmation",
            };
            context.Statuses.Add(testStatusWaiting);

            #region Orders
            Order testOrder = new Order()
            {
                Tickets = new List<Ticket>
                {
                    testTicket,
                    //testTicket2
                },
                TrackNO = 12762,
                Buyer = testUser2,
                OrderStatus = testStatusWaiting
            };
            context.Orders.Add(testOrder);
            #endregion

            context.SaveChanges();

        }
    }
}
