﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpamTask1.Models
{
    public class EventContext : IdentityDbContext<User>
    //public class EventContext : DbContext
    {
        public DbSet<Event> Events { get; set; }
        public DbSet<Ticket> Tickets { get; set; }
        //public DbSet<User> Users { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Venue> Venues { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<OrderStatus> Statuses { get; set; }

        public EventContext(DbContextOptions<EventContext> options) : base(options)
        {
        }
    }
}
