﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpamTask1.Models
{
    public class Event
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public string Banner { get; set; }

        public int? VenueId { get; set; }
        public Venue Venue { get; set; }

        public ICollection<Ticket> Tickets { get; set; }
        public Event()
        {
            Tickets = new List<Ticket>();
        }
    }
}
