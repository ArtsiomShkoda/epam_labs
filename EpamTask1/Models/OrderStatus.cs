﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpamTask1.Models
{
    public class OrderStatus
    {
        public int Id { get; set; }
        public string StatusType { get; set; }

        public ICollection<Order> Orders { get; set; }
        public OrderStatus()
        {
            Orders = new List<Order>();
        }
    }
}
