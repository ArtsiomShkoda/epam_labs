﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using EpamTask1.Models;
using Microsoft.EntityFrameworkCore;

namespace EpamTask1.Controllers
{
    public class EventController : Controller
    {
        private EventContext _context;

        public EventController(EventContext context)
        {
            _context = context;
        }
       
        public IActionResult Index(int? id)
        {
            IEnumerable<Event> allEvent = new List<Event>(_context.Events);
            return View(allEvent);
        }
    }
}
