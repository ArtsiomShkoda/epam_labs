﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using EpamTask1.Models;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace EpamTask1.Controllers
{
    [Authorize(Roles ="admin")]
    public class MyOrders : Controller
    {
        private EventContext _context;

        public MyOrders(EventContext context)
        {
            _context = context;
        }

        public IActionResult MyOrdersIndex()
        {
            return View();
        }


        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }
    }
}
