﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using EpamTask1.Models;
using Microsoft.EntityFrameworkCore;

namespace EpamTask1.Controllers
{
    public class TicketController : Controller
    {
        private EventContext _context;

        public TicketController(EventContext context)
        {
            _context = context;
        }
        // GET: /<controller>/
        public IActionResult Index(int? id)
        {
            IEnumerable<Ticket> allTickets;

            if (id != null)
            {
                allTickets = _context.Tickets
                    .Where(p => p.EventId == id)
                    .Where(p => p.Order == null)
                    .Include(t => t.Event)
                    .Include(t => t.Seller);

                ViewData["CurentEvent"] = _context.Events
                    .Include(p => p.Venue).ThenInclude(z => z.City).First(p => p.Id == id);
            }
            else
            {
                allTickets = _context.Tickets.Include(t => t.Event).Include(t => t.Seller);
            }

            return View(allTickets);
        }
    }
}
